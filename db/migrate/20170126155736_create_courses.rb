class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|
      t.string :Name
      t.string :SI_LEADER
      t.string :Department
      t.string :Venue
      t.text :Session_times

      t.timestamps
    end
  end
end
